#! /bin/sh
pygmentize -S emacs -f latex > pygmentize.py_tex

for source in `ls *.py`; do
pygmentize -f latex -O linenos=1 -l python \
    -o `basename $source .py`.py_tex $source
done

#for source in `ls *.c`; do
#pygmentize -f latex -O linenos=1 -l c \
#    -o `basename $source .c`.c_tex $source
#done

for source in `ls *.i`; do
pygmentize -f latex -O linenos=1 -l c \
    -o `basename $source .i`.i_tex $source
done

#for source in `ls *.cxx`; do
#pygmentize -f latex -O linenos=1 -l c++ \
#    -o `basename $source .cxx`.cxx_tex $source
#done

for source in `ls *.pyf`; do
pygmentize -f latex -O linenos=1 -l fortran \
    -o `basename $source .pyf`.pyf_tex $source
done
