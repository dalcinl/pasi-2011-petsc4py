all: pygmentize slides
pygmentize:
	./pygmentize.sh
slides:
	pdflatex -interaction=batchmode pasi2011.tex
	pdflatex -interaction=batchmode pasi2011.tex

clean:
	${RM} *.aux *.log *.fls *.nav *.snm *.out *.toc *.vrb

distclean: clean
	${RM} *.py_tex
	${RM} pasi2011.pdf
	${RM} .sconsign.dblite
