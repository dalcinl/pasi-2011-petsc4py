\documentclass{beamer}
%\usetheme{Warsaw}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{fancyvrb}
\input{pygmentize.py_tex}
\usepackage{xspace}
\newcommand{\Cpp}{C\protect\raisebox{.18ex}{++}\xspace}

\title[petsc4py]{PETSc for Python}
\subtitle{\url{http://petsc4py.googlecode.com}}
\author[L.~Dalcin]{Lisandro~Dalcin\\\url{dalcinl@gmail.com}}
\institute[]
{
  Centro Internacional de Métodos Computacionales en Ingeniería\\
  Consejo Nacional de Investigaciones Científicas y Técnicas\\
  Santa Fe, Argentina
}
\date [PASI '11]
{
  January, 2011\\
  Python for parallel scientific computing\\
  PASI, Valparaíso, Chile
}

\AtBeginSection[]
{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section*{Outline}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

% --- Overview ---

\section{Overview}

\begin{frame}
  \frametitle{What is \textbf{petsc4py}?}  

  Python bindings for \textbf{PETSc}, the \emph{Portable Extensible
    Toolkit for Scientific Computation}.  
  
  \bigskip
 
  A \emph{good friend} of \textbf{petsc4py} is:
  \begin{itemize}
  \item \textbf{mpi4py}: Python bindings for \textbf{MPI}, the
    \emph{Message Passing Interface}.
  \end{itemize}
  \medskip
  Other two projects depend on petsc4py:
  \begin{itemize}
  \item \textbf{slepc4py}: Python bindings for \textbf{SLEPc}, the
    \emph{Scalable Library for Eigenvalue Problem Computations}.
  \item \textbf{tao4py}: Python bindings for \textbf{TAO}, the
    \emph{Toolkit for Advanced Optimization}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Implementation}
  Implemented with Cython \url{http://www.cython.org}
  \begin{itemize}
  \item Code base far easier to write, maintain, and extend.
  \item Faster than other solutions (mixed Python and C codes).
  \item Easier to cross language boundaries (reuse C/\Cpp/Fortran).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- PETSc components}
  \begin{itemize}
  \item \textbf{Index Sets}: permutations, indexing into vectors, renumbering.
  \item \textbf{Vectors}: sequential and distributed.
  \item \textbf{Matrices}: sequential and distributed, sparse and dense.
  \item \textbf{Distributed Arrays}: regular grid-based problems.
  \item \textbf{Linear Solvers}: Krylov subspace methods.
  \item \textbf{Preconditioners}: sparse direct solvers, multigrid
  \item \textbf{Nonlinear Solvers}: line search, trust region, matrix-free.
  \item \textbf{Timesteppers}: time-dependent, linear and nonlinear PDE's.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- Interoperability}
  Support for wrapping other PETSc-based codes.
  \begin{itemize}
  \item You can use \textbf{SWIG} (\textsl{typemaps} provided).
  \item You can use \textbf{F2Py} (\texttt{fortran} attribute).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- Interoperability -- \textbf{SWIG} }
  \small
  \input{wrap_swig.i_tex}
\end{frame}

\begin{frame}
  \frametitle{Features -- Interoperability -- \textbf{F2Py} }
  \scriptsize
  \input{wrap_f2py.pyf_tex}
\end{frame}

% --- Vectors ---

\section{Vectors}

\begin{frame}
  \frametitle{Vectors (\texttt{Vec}) -- Conjugate Gradients Method}
  \begin{columns}[t]
    \begin{column}{.45\textwidth}
      \tiny
      \begin{equation*}
        \begin{split}
          & cg(A,x,b,i_{max},\epsilon): \\
          & \quad i \Leftarrow 0 \\
          & \quad r \Leftarrow b - A x \\
          & \quad d \Leftarrow r \\
          & \quad \delta_{0} \Leftarrow r^T r \\
          & \quad \delta_{   } \Leftarrow \delta_{0} \\
          & \quad \text{while}\;\; i < i_{max} \text{ and } \\
          & \quad\quad\qquad  \delta_{   } > \delta_{0} \epsilon^2 \text{ do} :\\
          & \quad\quad\quad  q \Leftarrow Ad \\
          & \quad\quad\quad  \alpha \Leftarrow \frac{\delta_{   }}{d^T q} \\
          & \quad\quad\quad  x \Leftarrow x + \alpha d\\
          & \quad\quad\quad  r \Leftarrow r - \alpha q\\
          & \quad\quad\quad  \delta_{old} \Leftarrow \delta_{   } \\
          & \quad\quad\quad  \delta_{   } \Leftarrow r^T r \\
          & \quad\quad\quad  \beta \Leftarrow \frac{\delta_{   }}{\delta_{old}} \\
          & \quad\quad\quad  d \Leftarrow r + \beta d\\
          & \quad\quad\quad  i \Leftarrow i + 1
        \end{split}
      \end{equation*}
    \end{column}
    \begin{column}{.45\textwidth}
      \tiny\input{petsc4py_vec.py_tex}
    \end{column}
  \end{columns}
\end{frame}

% --- Matrices ---

\section{Matrices}

\begin{frame}
  \frametitle{Matrices (\texttt{Mat}) [1]}
  \small\input{petsc4py_mat_1.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Matrices (\texttt{Mat}) [2]}
  \small\input{petsc4py_mat_2.py_tex}
\end{frame}

% --- Linear Solvers  ---

\section{Linear Solvers}

\begin{frame}
  \frametitle{Linear Solvers (\texttt{KSP+PC})}
  \small\input{petsc4py_ksp.py_tex}
\end{frame}

% --- Nonlinear Solvers  ---

\section{Nonlinear Solvers}

\begin{frame}
  \frametitle{Nonlinear Solvers (\texttt{SNES}) [1]}
  \scriptsize\input{petsc4py_snes_1.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Nonlinear Solvers (\texttt{SNES}) [2]}
  \small\input{petsc4py_snes_2.py_tex}
\end{frame}

% --- Closing ---

\section*{Closing}

\begin{frame}
  \Large
  Do not hesitate to ask for help \ldots
  \begin{itemize}
  \item Mailing List: \url{petsc-users@mcs.anl.gov}
  \item Mail\&Chat:   \url{dalcinl@gmail.com}
  \end{itemize}
  \bigskip
  \begin{centering}
    \Huge Thanks!\par
  \end{centering}
\end{frame}

\end{document}


% Local Variables:
% mode: latex
% TeX-PDF-mode: t
% End:
